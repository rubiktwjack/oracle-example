require("dotenv").config();
const web3 = require("./lib/web3.js").HTTPWeb3;
const path = require("path");

let contractData = require(path.resolve(
  __dirname,
  "build",
  "contracts",
  "OracleUser.json"
));

let OracleUserContract = new web3.eth.Contract(
  contractData.abi,
  process.env.ORACLEUSER_CONTRACT_ADDRESS
);

function askTWD(usd) {
  OracleUserContract.methods
  .buySomething()
  .send({
    from: process.env.ACCOUNT,
    gas: 1500000,
    gasPrice: "0"
  })
  .then(function(receipt) {
    setTimeout(function() {
      OracleUserContract.methods
        .exchangeRate()
        .call()
        .then(function(rate) {
          console.log(`匯率: ${rate / 100}`);
          console.log(`USD : ${usd}`)
          console.log(`TWD : ${usd * rate / 100}`)
        });
    }, 8000);
  });
}

if(!isNaN(process.argv[2])){
  askTWD(process.argv[2])
}
else {
  askTWD(10)
}