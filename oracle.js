require('dotenv').config()
const web3 = require('./lib/web3.js').WSWeb3
const path = require('path')
const rate = require('./lib/exchangeRate.js')

let contractData = require(path.resolve(__dirname, 'build', 'contracts', 'Oracle.json'))

let OracleContract = new web3.eth.Contract(contractData.abi, process.env.ORACLE_CONTRACT_ADDRESS)

OracleContract.events.NewRequest()
    .on('data', (event) => {
        rate.getUSDTWD()
            .then(function (USDTWD) {
                console.log(`UTC: ${USDTWD.UTC}`)
                console.log(`Exrate: ${USDTWD.Exrate}`)
                return OracleContract.methods.reply(event.returnValues[0], parseInt(USDTWD.Exrate * 100))
                    .send({
                        from: process.env.ACCOUNT,
                        gas: 1500000,
                        gasPrice: '0'
                    })
            })
            .then(function(receipt){
                console.log(`transactionHash: ${receipt.transactionHash}`)
            })
    })