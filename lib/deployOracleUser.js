require('dotenv').config()
const web3 = require('./web3.js').HTTPWeb3
const path = require('path')

let contractData = require(path.resolve(__dirname, '..', 'build', 'contracts', 'OracleUser.json'))

let OracleUserContract = new web3.eth.Contract(contractData.abi)

OracleUserContract.deploy({
    data: contractData.bytecode
})
.send({
    from: process.env.ACCOUNT,
    gas: 1500000,
    gasPrice: '0'
})
.on('error', console.error)
.on('receipt', (receipt) => {
   console.log(receipt.contractAddress)
})