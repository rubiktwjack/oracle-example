require('dotenv').config()
const web3 = require('./web3.js').HTTPWeb3
const path = require('path')

let contractData = require(path.resolve(__dirname, '..', 'build', 'contracts', 'Oracle.json'))

let OracleContract = new web3.eth.Contract(contractData.abi)

OracleContract.deploy({
    data: contractData.bytecode
})
.send({
    from: process.env.ACCOUNT,
    gas: 1500000,
    gasPrice: '0'
})
.on('error', console.error)
.on('receipt', (receipt) => {
   console.log(receipt.contractAddress)
})