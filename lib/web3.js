require("dotenv").config();
const Web3 = require("web3");
const WSWeb3 = new Web3(`ws://${process.env.PROVIDER_HOST}:8545`);
const HTTPWeb3 = new Web3(`http://${process.env.PROVIDER_HOST}:8545`);

module.exports = {
  WSWeb3,
  HTTPWeb3
};
