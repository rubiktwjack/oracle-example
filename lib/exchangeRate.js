const https = require('https');

// 此API為 https://tw.rter.info/howto_currencyapi.php 提供之服務

//取得美金台幣匯率
function getUSDTWD() {
  return new Promise(function (resolve, reject) {
    https.get('https://tw.rter.info/capi.php', (res) => {
      data = ""
      res.on('data', (d) => {
        data += d;
      });
      res.on('end', () => {
        resolve(JSON.parse(data).USDTWD)
      })
    }).on('error', (e) => {
      console.error(e);
    });
  })
}

module.exports = {
  getUSDTWD
}