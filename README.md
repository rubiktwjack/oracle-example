# oracle-test

## 環境設定

### 1. 設定.env
新增 .env file

    cp example.env .env


完成 .env (首先填入PROVIDER_HOST和ACCOUNT，其他項目於下一階段填入)

    PROVIDER_HOST = Web3 provider
    ORACLE_CONTRACT_ADDRESS = Oracle Contract Address
    ORACLEUSER_CONTRACT_ADDRESS = OracleUser Contract Address
    ACCOUNT = 發起交易的EOA

### 2. 編譯&部署合約(需要truffle)
    truffle compile
    node lib\deployOracle.js

將得到的Oracle合約地址記錄在 .env 中

並將Oracle合約地址寫入contracts\OracleUser.sol

範例:

    contract OracleUser {
      Oracle constant oracle = Oracle(0xEaa83EFF5a4748B7Ca53DD8da9cdf89D42EcE1e4); // known contract
      uint public exchangeRate;
      ...
    }

將Oracle合約地址填入後，重新編譯並部署合約

    truffle compile
    node lib\deployOracleUser.js

將得到的OracleUser合約地址記錄在 .env 中

## 使用說明

### 1. 開啟oracle監聽特定event
    node oracle.js 

    >>UTC: 2019-05-31 01:29:59
      Exrate: 31.599714
      transactionHash: 0xb45f0615178cf0e852bde35f3ef324713d6f0a488d43de4a2c2260f1ca44b618
### 2. 發出quert詢問匯率(約8秒後得到匯率)
    node query.js
    
    >>匯率: 31.59