pragma solidity >=0.4.22 <0.7.0;

contract Oracle {
  // 此合約的擁有者
  address payable owner;

  modifier isOwner() {
    require(owner == msg.sender, "you are not owner");
    _;
  }

  // 建構子
  constructor() public payable {
    owner = msg.sender;
  }

  struct Request {
    bytes data;
    function(uint) external callback;
  }
  Request[] requests;
  event NewRequest(uint);

  function query(bytes memory data, function(uint) external callback) public {
    requests.push(Request(data, callback));
    emit NewRequest(requests.length - 1);
  }
  
  function reply(uint requestID, uint response) public isOwner{
    requests[requestID].callback(response);
  }
}
