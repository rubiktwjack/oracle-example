pragma solidity >=0.4.22 <0.7.0;
import "./Oracle.sol";

contract OracleUser {
  Oracle constant oracle = Oracle(0xB97FfE2669102Da21f8F935b06413c3E6A23a54F); // known contract
  uint public exchangeRate;

  function buySomething() public returns(uint){
    oracle.query("USD", this.oracleResponse);
  }
  function oracleResponse(uint response) public {
    require(
        msg.sender == address(oracle),
        "Only oracle can call this."
    );
    exchangeRate = response;
  }
}